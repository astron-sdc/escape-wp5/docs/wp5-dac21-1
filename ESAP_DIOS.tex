\documentclass[pu]{escape}
\input{meta}
\input{changes}

\setDocNumber{WP5-1}
\setDocDate{\vcsDate}
\setDocTitle{The Data Lake in \glsentrytext{ESAP}}
\setWorkPackage{WP5}
\setLeadAuthor{Yan Grange (ASTRON)}
\setOtherAuthors{John Swinbank (ASTRON)}
\setDueDate{2021-11-12}
\setDueMonth{}

\newacronym{DAC21}{DAC~21}{Data Access Challenge 2021}
\newacronym{DLaaS}{DLaaS}{Data Lake as a Service}
\newacronym{DID}{DID}{Data IDentifier}
\newacronym{FUSE}{FUSE}{Filesystem in Userspace}
\newacronym{OIDC}{OIDC}{OpenID Connect}
\newacronym{FTS}{FTS}{File Transfer Service}
\newacronym{FAAI}{FAAI}{Federated Authentication and Authorization Infrastructure}

\begin{document}

\maketitle

\printglossary[title=List of Abbreviations]
\clearpage
\tableofcontents
\clearpage

\section{Context}

\Acrshort{ESCAPE} Work Package 2, \gls{DIOS}, is developing a ``scientific data lake'': a distributed system for storage and management of bulk scientific data sets\footnote{Note that this definition differs from the usage of the term ``data lake'' in cloud environments.
For brevity we will use the term ``data lake'' for the scientific data lake throughout this document.}.
This data lake consists of several components:
\begin{enumerate}
\item \gls{FTS}\footnote{\url{https://fts.web.cern.ch/fts/}}, which manages file transfers.
\item Indigo \gls{IAM}\footnote{\url{https://indigo-iam.github.io}} is the \gls{FAAI} service, which provides user and group management.
\item Rucio\footnote{\url{https://rucio.cern.ch/}}, which provides policy based data distribution across heterogeneous storage systems.
\end{enumerate}
The primary interface to the data lake is through Rucio.
This system is principally accessed through a \Acrshort{REST} \Acrshort{API}, which is used for communication by Rucio clients.

\Acrshort{ESCAPE} Work Package 5, \gls{ESAP}, is developing a toolkit which enables the construction of ``science platforms'': systems which integrate data discovery, access, and analysis, across multiple different resources, presenting the results to the end user through a consistent interface.
\gls{ESAP} is build around the concept of a ``shopping basket'', which contains the current working set of data of interest to the user, potentially harvested from and manipulated using a variety of different archives and systems.

This document describes plans for integrating the data lake with \Acrshort{ESAP}.
It particularly focuses on \gls{DAC21}, a major WP2-driven integration exercise scheduled for November 2021, but also provides ideas for future work.

\section{\glsentrylong{DLaaS}}

For the scientific end user, access to data in the data lake should be as transparent as possible.
To this end, the WP2 team at CERN have developed the \gls{DLaaS} system, which provides access to the data lake through a Jupyter\footnote{\url{https://jupyter.org/}} notebook plugin\footnote{\url{https://escape-notebook.cern.ch}}.
This notebook provides means to `stage' data sets to an area that is mounted over a networked \gls{FUSE} mount in the notebook, meaning applications can access the data as if it were on local disk.

\section{\glsentrylong{DAC21}}

In November 2021, various partners within work package 2 of ESCAPE will demonstrate how the data lake can be used to perform typical use cases which are coming from their domain and instrument as part of the \gls{DAC21} exercise.
This section describes the integration expected between \gls{ESAP} and the data lake in support of \gls{DAC21}.

\subsection{Features and requirements}
\label{sec:dac21:features}

\subsubsection{\glsentrytext{DLaaS} integration}

Within the context of \gls{DAC21}, we anticipate that \gls{ESAP} will be used to perform analysis that requires data from multiple sources, including data stored in the data lake.
To this end, ESAP should provide the capability to query for data in the data lake, put it in the shopping basket together with data from other sources, and access this data in the \gls{DLaaS} notebook for further processing.

The consequent requirements on \gls{ESAP} are:

\begin{enumerate}
    \item Query the data lake from \gls{ESAP} by scope and \gls{DID}\footnote{A \gls{DID} is the name that identifies an entry in the data lake, which could either be a single file, or a collection of files.}. \label{dac:qry}
    \item Read the Rucio data found in the previous step by using the \gls{ESAP} Python shopping basket plugin, and provide that to the \gls{DLaaS} functionality in a notebook. \label{dac:acs}
\end{enumerate}

Item \ref{dac:qry} has already been implemented.
The main implementation work should therefore focus on item \ref{dac:acs}.
The minimal way this needs to be implemented is by combining the shopping basket Python library, provided by WP5, with the \gls{DLaaS} notebook.
This would require copy-pasting the content of the \gls{JSON} in the \gls{DLaaS} query field.
Ideally however the \gls{DLaaS} service in the notebook would be able to directly read data from the shopping basket using the key \texttt{Rucio}.
It will then be directly made available to the user for processing.

\subsubsection{Multiple \glsentrytext{ESAP} instances}

Since the goal of \gls{ESAP} is to be a system that can be deployed and adapted by different projects, and the data lake has the ambition to support access by a broad range of users, \gls{DAC21} provides an important opportunity to demonstrate the flexibility of both \gls{ESAP} and \gls{DIOS}.
We therefore suggest that multiple projects should deploy \gls{ESAP} during \gls{DAC21}.
As per \cref{sec:dac21:delivery}, hardware provisioning and deployment during \gls{DAC21} is a WP2 responsibility.
However, WP5 will provide support by ensuring that the \gls{ESAP} codebase is technically capable of supporting multiple independent deployments of \gls{ESAP}.

\subsubsection{\glsentrytext{OIDC} authentication}

For \gls{DAC21} to be executed successfully from the \gls{ESAP} perspective, it is essential that \gls{OIDC} token-based authentication --- at least for the Rucio catalogue --- is in place.
This means that applying rules, querying the data in the data lake, and other catalogue-focused activities need to be possible using authentication with an \gls{OIDC} token.

For direct data access --- uploads and downloads between local and remote storage --- \gls{OIDC} tokens are still the strongly preferred option.
However, the data lake is heterogeneous by design, consisting different storage middleware systems.
Implementation of \gls{OIDC} authentication would require actions from development teams that are not part of the project.
Therefore, data access using X.509 certificates can be a fall-back.

\subsection{Delivery and deployment}
\label{sec:dac21:delivery}

Code which provides the features described in \cref{sec:dac21:features} will be committed by WP5 members to \gls{ESAP} code repositories on \texttt{git.astron.nl} in advance of the start of \gls{DAC21}.
WP5 members will provide best-efforts support to members of WP2 or other \gls{DAC21} participants in using \gls{ESAP}, both generally and with specific attention to this functionality.

WP2 members and other \gls{DAC21} participants are responsible for deploying and using \gls{ESAP} in the context of \gls{DAC21}, including provisioning whatever hardware systems on which those deployments are performed.

\section{Subsequent work}

After \gls{DAC21}, further development could focus on offering querying functionality that is more complex than just file names.
This depends on the implementation of custom metadata for data in the Rucio installation by the WP2 team.
When this is available, it there is an excellent opportunity to demonstrate how \gls{ESAP} and \gls{DIOS} can interoperate to support enhanced data findability.

\end{document}
