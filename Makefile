DOCHANDLE=ESAP_DIOS
export TEXMFHOME ?= astron-texmf/texmf

$(DOCHANDLE).pdf: ESAP_DIOS.tex meta.tex changes.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) ESAP_DIOS.tex
	makeglossaries $(subst .,_,$(DOCHANDLE))
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) ESAP_DIOS.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) ESAP_DIOS.tex

include astron-texmf/make/vcs-meta.make
include astron-texmf/make/changes.make
